# Surveystack Kit

Native connectivity to devices for [SurveyStack.io](https://app.surveystack.io)

![Screenshot](./screen.png)


## Using the Application

1. When encountering a native script type question on surveystack.io, the Android app will prompt to run a measurement.
2. Follow the instructions of the Android App to connect to the device and run the measurement.
3. The result will be sent to surveystack.io and be included in your submission!