package io.surveystack.app.repository

import io.surveystack.app.base.App
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject
import org.oursci.android.common.measurement.ScriptBase
import timber.log.Timber
import java.io.BufferedReader
import java.net.URLEncoder
import java.nio.charset.Charset

class Repository(val app: App) {

    val client = OkHttpClient()

    data class RepoItem(
        override val id: String,
        override val name: String,
        val type: String,
        val path: String,
        val mode: String,
        val url: String,
        val sha256: String,
    ) : ScriptBase()


    companion object {
        private val lock = Object()
        private var sInstance: Repository? = null

        fun of(app: App): Repository {
            synchronized(lock) {
                if (sInstance == null) {
                    sInstance = Repository(app)
                }
                return sInstance!!
            }
        }
    }

    fun fetchCurrentCommitHash(projectId: String): String {
        val url = "https://gitlab.com/api/v4/projects/$projectId/repository/branches/master"
        val request = Request.Builder()
            .url(url).build()
        val inputStream = client.newCall(request).execute().body()?.byteStream() ?: throw Exception(
            "unable to fetch gitlab tree"
        )

        val text = inputStream.use {
            BufferedReader(it.bufferedReader(Charset.forName("UTF-8"))).readText()
        }

        val json = JSONObject(text)

        return json.getJSONObject("commit").getString("id")
    }

    fun fetch(projectId: String): List<RepoItem> {

        val url = "https://gitlab.com/api/v4/projects/$projectId/repository/tree?per_page=100"
        val request = Request.Builder()
            .url(url).build()
        val inputStream = client.newCall(request).execute().body()?.byteStream() ?: throw Exception(
            "unable to fetch gitlab tree"
        )

        val text = inputStream.use {
            BufferedReader(it.bufferedReader(Charset.forName("UTF-8"))).readText()
        }

        val json = JSONArray(text)

        val res = mutableListOf<RepoItem>()

        Timber.d("files: ${json.length()}")

        for (idx in 0 until json.length()) {
            val obj = json.getJSONObject(idx)

            val fileUrl =
                "https://gitlab.com/api/v4/projects/${projectId}/repository/files/${
                    URLEncoder.encode(
                        obj.getString("path"),
                        Charsets.UTF_8.name()
                    )
                }"

            // Timber.d("file url $fileUrl")
            val fileRequest = Request.Builder()
                .url("${fileUrl}?ref=master").head().build()

            val sha256 = try {
                client.newCall(fileRequest).execute().use {
                    return@use it.header("X-Gitlab-Content-Sha256")
                } ?: ""
            } catch (e: Exception) {
                e.printStackTrace()
                ""
            }

            // Timber.d("sha $sha256")

            res.add(
                RepoItem(
                    id = obj.getString("id"),
                    name = obj.getString("name"),
                    type = obj.getString("type"),
                    path = obj.getString("path"),
                    mode = obj.getString("mode"),
                    url = fileUrl,
                    sha256 = sha256
                )
            )
        }

        return res
    }


}