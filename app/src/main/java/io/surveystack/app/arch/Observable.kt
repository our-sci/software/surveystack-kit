package io.surveystack.app.arch;

import java.util.*
import java.util.Observable

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 14.09.19.
 */
@Suppress("UNCHECKED_CAST")
class Observable<T> : Observable() {
    fun update(value: T) {
        setChanged()
        notifyObservers(value)
    }

    fun observe(cb: (arg: T) -> Unit) {

        deleteObservers()

        val observer = object : Observer {
            override fun update(o: Observable?, arg: Any?) {
                val argument = arg as T
                cb(argument)
                deleteObserver(this)
            }

        }

        addObserver(observer)
    }


}