package io.surveystack.app.arch;

import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import java.util.concurrent.atomic.AtomicBoolean;

public class ConsumableLiveData<T> extends MutableLiveData<T> {
    private static final String TAG = "ConsumableLiveData";

    private final AtomicBoolean mConsumed = new AtomicBoolean(false);

    @MainThread
    public void observe(LifecycleOwner owner, final ConsumableObserver<T> observer) {
        // Observe the internal MutableLiveData
        super.observe(owner, new Observer<T>() {
            @Override
            public void onChanged(@Nullable T t) {
                observer.onChanged(t, mConsumed.getAndSet(true));
            }
        });
    }

    @MainThread
    public void setValue(@Nullable T t) {
        mConsumed.set(false);
        super.setValue(t);
    }

    /**
     * Used for cases where T is Void, to make calls cleaner.
     */
    @MainThread
    public void call() {
        setValue(null);
    }
}
