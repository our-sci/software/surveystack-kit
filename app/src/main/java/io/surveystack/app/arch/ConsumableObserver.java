package io.surveystack.app.arch;

import androidx.annotation.Nullable;

public interface ConsumableObserver<T> {
    void onChanged(@Nullable T t, boolean consumed);


}
