package io.surveystack.app.webserver

import fi.iki.elonen.NanoHTTPD
import io.surveystack.app.base.App
import io.surveystack.app.utils.Compression
import io.surveystack.app.measurement.MeasurementLoader
import timber.log.Timber
import java.io.File

class WebServer(val app: App) : NanoHTTPD("0.0.0.0", App.APP_WEBSERVER_PORT) {

    private var mMostRecentResult: String = ""
    var mInput: String = "{}"

    companion object {
        var instance: WebServer? = null
        fun of(app: App): WebServer {
            if (instance == null) {
                instance = WebServer(app)
            }

            return instance!!
        }
    }


    override fun serve(session: IHTTPSession?): Response {
        if (session == null) {
            return super.serve(session)
        }

        if (session.uri.endsWith("/upload_measurement")) {

            val tmpDir = File(app.cacheDir, "upload_measurement")
            if (!tmpDir.deleteRecursively()) {
                Timber.e("error removing tmpdir recursively ${tmpDir.absolutePath}")
            }


            if (!tmpDir.exists()) {
                if (!tmpDir.mkdirs()) {
                    Timber.e("unable to create directory %s", tmpDir.absolutePath)
                    return fail()
                }
            }

            if (!tmpDir.isDirectory) {
                Timber.e("not a directory %s", tmpDir.absolutePath)
                return fail()
            }


            try {
                val tmpFile = File(app.cacheDir, "upload_file.zip")
                tmpFile.deleteOnExit()

                if (tmpFile.exists()) {
                    if (!tmpFile.delete()) {
                        val reason = "unable to delete temp zip file ${tmpFile.absolutePath}"
                        Timber.e(reason)
                        app.onMeasurementLoadError(reason)
                        return fail(reason)
                    }
                }

                var remain: Int = session.headers["content-length"]?.toIntOrNull() ?: -1
                if (remain == -1) {
                    val reason = "unable to parse content length"
                    Timber.e(reason)
                    app.onMeasurementLoadError(reason)
                    return fail(reason)
                }

                val block = 4096
                val buffer = ByteArray(block)

                Timber.d("reading len bytes: %d", remain)

                try {
                    session.inputStream.use {
                        tmpFile.outputStream().use {
                            while (true) {
                                val next = if (remain >= block) block else remain
                                val len = session.inputStream.read(buffer, 0, next)
                                remain -= len
                                it.write(buffer, 0, len)

                                if (remain <= 0) {
                                    break
                                }
                            }
                        }
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                    val reason =
                        "error saving stream to zip file ${tmpFile.absolutePath}, ${e.message}"
                    app.onMeasurementLoadError(reason)
                    return fail(reason)
                }

                Timber.d("tmp file size: ${tmpFile.length()}")


                Compression().unzip(tmpFile.inputStream(), tmpDir)
                MeasurementLoader.of(app).load(tmpDir, { measurement ->
                    app.onMeasurementScriptUploaded(measurement)
                }, { error ->
                    app.onMeasurementLoadError(error)
                })
            } catch (e: Exception) {
                Timber.e(e)
                app.onMeasurementLoadError("unable to decompress http stream: ${e.message}")
                return fail("error extracting zip")
            }

            val resp = newFixedLengthResponse("ok")
            resp.addHeader("Content-Type", "text/plain")
            return resp
        } else if (session.uri.endsWith("/input.html")) {
            val t = app.assets.open("webserver/input.html").bufferedReader().use { it.readText() }
            return newFixedLengthResponse(t)
        } else if (session.uri.endsWith("/answer")) {
            val map = HashMap<String, String>()

            session.parseBody(map)
            map["postData"]?.let(app::answerFromWebserver)


            return newFixedLengthResponse("ok")
        } else if (session.uri.endsWith("/measurement")) {
            val resp = newFixedLengthResponse(mMostRecentResult)
            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.addHeader("Access-Control-Max-Age", "3628800");
            resp.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
            resp.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
            resp.addHeader("Access-Control-Allow-Headers", "Authorization");
            return resp
        } else if (session.uri.endsWith("/input")) {
            Timber.d("input request received")


            val resp = newFixedLengthResponse(Response.Status.OK, MIME_PLAINTEXT, "success");
            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.addHeader("Access-Control-Max-Age", "3628800");
            resp.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
            resp.addHeader("Access-Control-Allow-Headers", "*");


            resp.setChunkedTransfer(true);

            if (session.method == Method.OPTIONS) {
                return resp
            }

            val map = HashMap<String, String>()
            session.parseBody(map)
            mInput = map["postData"] ?: ""

            Timber.d("method: ${session.method}")
            Timber.d("parsed input $mInput")

            return resp
        }
        return newFixedLengthResponse("<html><body>Hello World!</body></html>\n")
    }

    private fun fail(reason: String = ""): Response {
        val resp = newFixedLengthResponse(
            if (reason.isEmpty()) "error" else "error: $reason"
        )
        resp.addHeader("Content-Type", "text/plain")
        return resp
    }

    fun publishResult(result: String) {
        mMostRecentResult = result
    }
}