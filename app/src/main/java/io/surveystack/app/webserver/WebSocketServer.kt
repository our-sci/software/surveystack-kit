package io.surveystack.app.webserver

import fi.iki.elonen.NanoWSD
import io.surveystack.app.base.App
import kotlinx.coroutines.withTimeout
import timber.log.Timber
import java.io.IOException
import kotlin.concurrent.thread


class WebSocketServer(val app: App) : NanoWSD("0.0.0.0", App.APP_WEBSERVER_PORT + 1) {

    private val sockets = mutableListOf<WebSocket>()

    val keepAlive = thread(name = "Websocket Keepalive Thread") {
        while (true) {
            synchronized(this) {
                sockets.forEach {
                    if (it.isOpen) {
                        it.send("ping")
                    }
                }
            }

            Thread.sleep(2000)
        }
    }

    class WebSocket(handshakeRequest: IHTTPSession?, val uninstall: (WebSocket) -> (Unit)) :
        NanoWSD.WebSocket(handshakeRequest) {
        override fun onOpen() {
            Timber.d("websocked opened")
            send("ping")
        }

        override fun onClose(
            code: WebSocketFrame.CloseCode?,
            reason: String?,
            initiatedByRemote: Boolean
        ) {
            Timber.d("closing socket")
            uninstall(this)
        }

        override fun onMessage(message: WebSocketFrame?) {
            Timber.d("on message")
        }

        override fun onPong(pong: WebSocketFrame?) {
            Timber.d("on pong")
        }

        override fun onException(exception: IOException?) {
            Timber.e(exception)
            uninstall(this)
        }

    }

    companion object {
        var instance: WebSocketServer? = null
        fun of(app: App): WebSocketServer {
            if (instance == null) {
                instance = WebSocketServer(app)
            }

            return instance!!
        }
    }

    override fun openWebSocket(handshake: IHTTPSession?): WebSocket {
        val s = WebSocket(handshake) { socket ->
            Timber.d("removing socket")
            synchronized(this) {
                sockets.remove(socket)
            }

        }
        synchronized(this) {
            sockets.add(s)
        }

        return s
    }

    fun done(cb: () -> Unit) {
        Timber.d("sending to ${sockets.size} sockets")
        thread {
            synchronized(this) {
                sockets.forEach { it.send("done") }
            }
            cb()
        }
    }
}