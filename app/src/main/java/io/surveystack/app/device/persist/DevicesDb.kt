package io.surveystack.app.device.persist

import androidx.room.Room
import io.surveystack.app.base.App

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 13.02.19.
 */
class DevicesDb(val app: App) {
    val db by lazy {
        Room.databaseBuilder(app, DevicesDatabase::class.java,
                "devices_database")
                .allowMainThreadQueries()
                .build()
    }


    companion object {

        private val lock = Object()

        private var sInstance: DevicesDb? = null

        fun of(app: App): DevicesDb {
            synchronized(lock) {
                val instance = sInstance ?: DevicesDb(app)
                sInstance = instance
                return instance
            }
        }
    }
}