package io.surveystack.app.device.persist

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 13.02.19.
 */


@Entity
data class KnownBluetooth(
        @PrimaryKey var mac: String,
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "last_connected") var lastConnected: Long,
        @ColumnInfo(name = "device_info") var deviceInfo: String = ""
)