package io.surveystack.app.device.persist


import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 13.02.19.
 */

@Database(
    entities = arrayOf(KnownBluetooth::class),
    version = 1,
    exportSchema = false,
)
abstract class DevicesDatabase : RoomDatabase() {
    abstract fun knownBluetoothDao(): KnownBluetoothDao
}