package io.surveystack.app.device.persist

import androidx.room.*
import io.surveystack.app.device.persist.KnownBluetooth

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 13.02.19.
 */
@Dao
interface KnownBluetoothDao {

    @Query("SELECT * from KnownBluetooth ORDER BY last_connected DESC")
    fun getAll(): List<KnownBluetooth>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKnownBluetooth(knownBluetooth: KnownBluetooth)

    @Delete
    fun deleteKnownBluetooth(knownBluetooth: KnownBluetooth)


}