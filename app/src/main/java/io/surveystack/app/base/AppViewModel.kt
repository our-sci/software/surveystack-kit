package io.surveystack.app.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel

open class AppViewModel(application: Application) : AndroidViewModel(application) {
    val app by lazy { getApplication() as App }
}