package io.surveystack.app.base

import android.app.Application
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.preference.PreferenceManager
import androidx.core.content.edit
import io.surveystack.app.arch.ConsumableLiveData
import io.surveystack.app.device.bt.BtManager
import io.surveystack.app.device.usb.UManager
import io.surveystack.app.webserver.WebServer
import io.surveystack.app.webserver.WebSocketServer
import org.oursci.android.common.measurement.MeasurementScript
import timber.log.Timber
import java.io.File
import java.util.concurrent.atomic.AtomicBoolean

class App : Application() {
    companion object {
        const val APP_WEBSERVER_PORT = 7375
    }

    private val filesDirName = "surveystack-kit"

    lateinit var appFilesDir: File

    val ACTION_USB_PERMISSION = "org.oursci.android.common.ACTION_USB_PERMISSION"

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        appFilesDir = File(filesDir, filesDirName)

        Timber.d("using files dir: ${appFilesDir.absolutePath}")
        BtManager.of(this).init()
        UManager.of(this).init()
        WebServer.of(this).start()
        WebSocketServer.of(this).start(0)

        registerReceiver(mUsbReceiver, IntentFilter(ACTION_USB_PERMISSION))
    }


    fun trashDir() = File(appFilesDir, "deleted_surveys")
    fun trashMeasurementsDir() = File(appFilesDir, "deleted_measurements")


    fun makeFilesDir() {
        try {
            appFilesDir.mkdirs()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }


    private val mUsbReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                val action = it.action ?: return

                if (action != ACTION_USB_PERMISSION) {
                    return
                }

                val device = it.getParcelableExtra<UsbDevice>(UsbManager.EXTRA_DEVICE)

                if (it.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    usbPermissionCallback?.invoke(device)
                } else {
                    Timber.e("unable to get devcie from intent")
                    usbPermissionCallback?.invoke(null)
                }
            }
        }

    }


    var usbPermissionCallback: ((device: UsbDevice?) -> Unit)? = null

    fun registerPermissionCallback(callback: (device: UsbDevice?) -> Unit) {
        usbPermissionCallback = callback
    }

    private val rememberMap = mutableMapOf<String, String?>()


    private val anonmap = mutableMapOf<String, Boolean>()

    data class PersistedOrg(val id: String, val name: String, val desc: String)


    val defaultOursciDevices = listOf(
        "reflectometer"
    )

    fun isOurSciDevice(): Boolean {
        val prefName = currentDeviceInfo()?.let(::prefNameFromDevcie) ?: return false
        val deviceName = (currentDeviceInfo() as? BluetoothDevice)?.name ?: ""
        return PreferenceManager.getDefaultSharedPreferences(this)
            .getBoolean(prefName, defaultOursciDevices.contains(deviceName))
    }

    fun setOurSciDevice(b: Boolean) {
        val prefName = currentDeviceInfo()?.let(::prefNameFromDevcie) ?: return
        PreferenceManager.getDefaultSharedPreferences(this).edit {
            putBoolean(prefName, b)
        }
    }

    private fun prefNameFromDevcie(device: Any) = when (device) {
        is BluetoothDevice -> {
            "bt_${device.address}"
        }
        is UsbDevice -> {
            "usb_%04X_%04X".format(device.vendorId, device.productId)
        }
        else -> {
            null
        }
    }

    private fun currentDeviceInfo() = if (BtManager.of(this).hasDevice()) {
        BtManager.of(this).currentDevice()
    } else {
        UManager.of(this).currentDevice()
    }


    val webAnswerData = ConsumableLiveData<String>()


    fun answerFromWebserver(answer: String) {
        Timber.d("received answer from webserver: %s", answer)
        webAnswerData.postValue(answer)
    }

    fun onMeasurementLoadError(reason: String) {
        TODO("Not yet implemented")
    }

    fun onMeasurementScriptUploaded(measurement: MeasurementScript) {
        TODO("Not yet implemented")
    }

    fun onSurveyLoadError(reason: String) {
        TODO("Not yet implemented")
    }


    var online = AtomicBoolean(true)


}
