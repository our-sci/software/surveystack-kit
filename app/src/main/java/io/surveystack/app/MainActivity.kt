package io.surveystack.app

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Build.VERSION_CODES.S
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import com.google.android.material.bottomnavigation.BottomNavigationView
import io.surveystack.app.base.App
import io.surveystack.app.fragments.HomeViewModel
import io.surveystack.app.fragments.script.ScriptMainViewModel
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    companion object {
        const val RC_BLUETOOTH_ENABLE = 2000
        const val RC_BLUETOOTH_ENABLE_FROM_MEASUREMENT = 2100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)

        navView.setOnNavigationItemSelectedListener {
            Timber.d("item clicked")
            when (it.itemId) {
                R.id.navigation_home_button -> {
                    Timber.d("popping backstack to script main")
                    val r = navController.popBackStack(R.id.scriptMain, false)
                    if (!r) {
                        navController.popBackStack(R.id.navigation_home, false)
                    }
                    title = "Run"
                }
                R.id.navigation_device_button -> {
                    title = "Devices"
                    val app = this@MainActivity.applicationContext as App

                    /* if (UManager.of(app).hasDevice() || BtManager.of(app).hasDevice()) {
                        navController.navigate(R.id.deviceSettings)
                    } else {
                        navController.navigate(R.id.navigation_device)
                    } */

                    val opts = NavOptions.Builder().setPopUpTo(R.id.navigation_device, true).build()
                    navController.navigate(R.id.navigation_device, null, opts)


                }
            }
            true
        }


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home_button, R.id.navigation_device_button
            )
        )
        //setupActionBarWithNavController(navController, appBarConfiguration)

        title = "Run"

        if (requiresPermission()) {
            showPermissionDialog(
                "App requires Permmission",
                "In order to scan, acquire and process data, a couple of " +
                        "permissions are required."
            )
        }
        // navView.setupWithNavController(navController)
        // navController.handleDeepLink(intent)

        handleIntent(intent)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        Timber.d("creating options menu")
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.navigation_settings) {
            findNavController(R.id.nav_host_fragment).navigate(R.id.open_settings_fragment)
        }
        return true
    }

    private val basePermissions = mutableListOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_NETWORK_STATE,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.BLUETOOTH,
        Manifest.permission.BLUETOOTH_ADMIN,
// for the time beeing we don't need audio
//   Manifest.permission.RECORD_AUDIO
    )
    private val permissions =
        if (Build.VERSION.SDK_INT < S) basePermissions else basePermissions.apply {
            add(Manifest.permission.BLUETOOTH_CONNECT)
            add(Manifest.permission.BLUETOOTH_SCAN)
        }

    var permissionSuccess: (() -> Unit)? = null
    var permissionFailure: (() -> Unit)? = null

    fun requiresPermission() = permissions.any {
        Timber.d("%s %d", it, ContextCompat.checkSelfPermission(this, it))
        ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED
    }

    fun showPermissionDialog(
        title: String,
        message: String,
        positive: () -> Unit = {},
        negative: () -> Unit = {}
    ) {

        permissionSuccess = positive
        permissionFailure = negative

        if (!requiresPermission()) {
            positive()
            return
        }

        val alertDialog = AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Yes Allow") { dialog, _ ->
                dialog.dismiss()
                requestPermissions(permissions)
            }
            .setNegativeButton("Maybe later") { dialog, _ ->
                dialog.dismiss()
                negative()
            }
            .create()
        alertDialog.show()
    }

    private val PERMISSION_REQUEST: Int = 1

    fun requestPermissions(permissions: List<String>) {
        ActivityCompat.requestPermissions(
            this,
            permissions.toTypedArray(),
            PERMISSION_REQUEST
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_REQUEST -> {
                if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                    (applicationContext as App).makeFilesDir()
                    permissionSuccess?.let {
                        it()
                    }
                } else {
                    permissionFailure?.let { it() }
                }
            }
        }
    }


    fun handleIntent(intent: Intent?) {
        val navController = findNavController(R.id.nav_host_fragment)

        intent?.let {
            Timber.d("Data String ${it.dataString}")
            val d = it.dataString ?: return
            try {
                val (_, id) = d.split("://kit/")
                val viewModel = ViewModelProvider(this).get(ScriptMainViewModel::class.java)
                viewModel.autorun = true
                navController.navigate(
                    R.id.action_navigation_home_to_scriptMain,
                    bundleOf("id" to id)
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }


}