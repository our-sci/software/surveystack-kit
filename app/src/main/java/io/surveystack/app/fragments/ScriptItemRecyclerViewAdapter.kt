package io.surveystack.app.fragments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.surveystack.app.databinding.FragmentItemBinding
import org.oursci.android.common.measurement.ScriptBase

class ScriptItemRecyclerViewAdapter(
    val values: MutableList<ScriptBase> = mutableListOf(),
    val listener: (ScriptBase) -> (Unit)
) : RecyclerView.Adapter<ScriptItemRecyclerViewAdapter.ViewHolder>() {

    lateinit var binding: FragmentItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = FragmentItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.binding.script = item
        holder.binding.btScript.setOnClickListener {
            listener(item)
        }
        holder.binding.invalidateAll()
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: FragmentItemBinding) : RecyclerView.ViewHolder(binding.root)
}
