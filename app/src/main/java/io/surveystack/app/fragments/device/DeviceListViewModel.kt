package io.surveystack.app.fragments.device

import android.app.Application
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.lifecycle.AndroidViewModel
import io.surveystack.app.R
import io.surveystack.app.arch.ConsumableLiveData
import io.surveystack.app.base.App
import io.surveystack.app.btlib.BluetoothStatus
import io.surveystack.app.device.bt.BtManager
import io.surveystack.app.device.persist.DevicesDb
import io.surveystack.app.device.persist.KnownBluetooth
import io.surveystack.app.device.usb.UManager
import org.joda.time.DateTime


import timber.log.Timber
import java.nio.charset.Charset

class DeviceListViewModel(application: Application) : AndroidViewModel(application) {
    // TODO: Implement the ViewModel

    // deviceCache contains a list of Pair<Any, DeviceItemDisplay>
    var deviceCache = mutableSetOf<Pair<Any, DeviceItemDisplay>>()


    val deviceListResult = ConsumableLiveData<List<Pair<Any, DeviceItemDisplay>>>()
    val deviceConnectedResult = ConsumableLiveData<String>()
    val deviceScanDone = ConsumableLiveData<Void>()


    private var scanning = false
    private var pairing = false;


    private var deviceToConnect: BluetoothDevice? = null

    private val bluetoothLeScanner = BluetoothAdapter.getDefaultAdapter().bluetoothLeScanner

    val scanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            val device = result.device
            super.onScanResult(callbackType, result)
            val state = when (device.bondState) {
                BluetoothDevice.BOND_BONDED -> DeviceItemDisplay.State.PAIRED
                BluetoothDevice.BOND_BONDING -> DeviceItemDisplay.State.CONNECTING
                else -> DeviceItemDisplay.State.UNPAIRED
            }
            val display = DeviceItemDisplay(
                device, device.name
                    ?: "unknown name", device.address, iconBt, state
            )


            deviceCache.add(Pair(device, display))
        }
    }

    private fun scanLeDevice() {

    }

    private fun stopLeScan() {
        bluetoothLeScanner.stopScan(scanCallback)
        scanning = false

        deviceListResult.postValue(createDeviceList())
    }

    fun startDeviceScan() {
        scanning = true
        deviceCache.clear()


        UManager.of(getApplication()).scan()
        //BtManager.of(getApplication()).scan()
    }

    fun stopDeviceScan() {
        BtManager.of(getApplication()).stopScan()
        scanning = false
    }

    fun clearCache() {
        synchronized(deviceCache) {
            deviceCache.clear()
            deviceListResult.postValue(createDeviceList())
        }
    }

    fun connect(device: UsbDevice) {
        Timber.d("connect usb device %s", device::class.java)
        UManager.of(getApplication()).apply {
            if (hasPermission(device)) {
                Timber.d("connecting to usb device: permission has been granted")
                connect(device)
                deviceConnectedResult.postValue(if (connected.get()) device.deviceName else null)
                deviceListResult.postValue(createDeviceList())
            } else {
                Timber.d("requesting permission for usb device")
                requestPermission(device) { device ->
                    if (device == null) {
                        deviceConnectedResult.postValue(null)
                    } else {
                        connect(device)
                        deviceConnectedResult.postValue(if (connected.get()) device.deviceName else null)
                    }
                    deviceListResult.postValue(createDeviceList())
                }
            }
        }
    }

    fun connect(device: BluetoothDevice): Boolean {
        deviceToConnect = device

        if (!BluetoothAdapter.getDefaultAdapter().bondedDevices.contains(device)) {
            return false
        }

        BtManager.of(getApplication()).connect(device)
        return true
    }

    fun disconnect() {
        BtManager.of(getApplication()).cancelConnect(null)
    }

    fun disconnectUSB() {
        UManager.of(getApplication()).cancelConnect(null)
        deviceListResult.postValue(createDeviceList())
    }

    private val iconBt by lazy {
        colorIcon(R.drawable.ic_bluetooth_black_24dp)
    }

    private val iconUsb by lazy {
        colorIcon(R.drawable.ic_usb_black_24dp)
    }


    private fun colorIcon(icon: Int) = AppCompatResources.getDrawable(getApplication(), icon)?.let {
        DrawableCompat.setTint(it, ContextCompat.getColor(getApplication(), R.color.white))
        it
    }


    fun setupCallbacks() {
        BtManager.of(getApplication()).registerCallback({ device ->
            synchronized(deviceCache) {
                val state = when (device.bondState) {
                    BluetoothDevice.BOND_BONDED -> DeviceItemDisplay.State.PAIRED
                    BluetoothDevice.BOND_BONDING -> DeviceItemDisplay.State.CONNECTING
                    else -> DeviceItemDisplay.State.UNPAIRED
                }
                val display = DeviceItemDisplay(
                    device, device.name
                        ?: "unknown name", device.address, iconBt, state
                )


                deviceCache.add(Pair(device, display))
                deviceListResult.postValue(createDeviceList())
            }
        }, { status ->
            when (status) {
                BluetoothStatus.CONNECTED -> {
                    (getApplication() as App).setOurSciDevice(true) // TODO: figure out if this is an oursci device
                    deviceConnectedResult.postValue(deviceToConnect?.name)
                    addKnownBluetoothDevice(deviceToConnect)
                    deviceToConnect = null
                }
                BluetoothStatus.CONNECTING -> Timber.d("status connecting")
                BluetoothStatus.NONE -> {
                    deviceToConnect = null
                    deviceConnectedResult.postValue(null)
                }
            }
            deviceListResult.postValue(createDeviceList())

        }, { data ->
            Timber.d("data of device: %s", data)
        }, { bondedDevices ->
            scanning = false
            bondedDevices.forEach { device ->
                val display = DeviceItemDisplay(
                    device, device.name
                        ?: "unknown name", device.address, iconBt, DeviceItemDisplay.State.KNOWN
                )
                deviceCache.add(Pair(device, display))
            }
            deviceListResult.postValue(createDeviceList())
            deviceScanDone.postValue(null)
        })

        UManager.of(getApplication()).registerCallback({ usbDevice: UsbDevice ->
            synchronized(deviceCache) {
                val display = DeviceItemDisplay(
                    usbDevice, usbDevice.deviceName, "%04X:%04X\n%s".format(
                        usbDevice.vendorId, usbDevice.productId,
                        UManager.of(getApplication()).info(
                            usbDevice.vendorId, usbDevice.productId
                        )
                    ), iconUsb
                )
                deviceCache.add(Pair(usbDevice, display))
                deviceListResult.postValue(createDeviceList())
            }
        }, { status ->
        }, { data ->
            Timber.d("received data from usb device: %s", data)
        }, {

        })

        deviceListResult.postValue(createDeviceList())
    }

    fun addKnownBluetoothDevice(device: BluetoothDevice?) {
        val kb = KnownBluetooth(
            device?.address
                ?: "??:??:??:??:??:??", device?.name
                ?: "unknown name", DateTime().millis, ""
        )
        DevicesDb.of(getApplication()).db.knownBluetoothDao().insertKnownBluetooth(kb)
        deviceListResult.postValue(createDeviceList())
    }

    fun removeKnownBluetoothDevice(device: KnownBluetooth) {
        DevicesDb.of(getApplication()).db.knownBluetoothDao().deleteKnownBluetooth(device)
        deviceListResult.postValue(createDeviceList())
    }

    fun isScanning(): Boolean {
        return scanning
    }

    fun cancelConnect() {
        UManager.of(getApplication()).clearDevice()
        BtManager.of(getApplication()).cancelConnect(deviceToConnect)
    }

    fun isConnecting() = deviceToConnect != null

    fun isPairing() = pairing

    fun bond(device: BluetoothDevice) {
        pairing = true
        deviceToConnect = device
        BtManager.of(getApplication()).stopScan()
        device.setPin("1234".toByteArray(Charset.forName("UTF-8")))
        device.createBond()

        try {
            (getApplication() as App).registerReceiver(
                pairingReceiver,
                IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
            )
        } catch (e: Exception) {
            Timber.e(e)
        }
    }


    fun createDeviceList(): List<Pair<Any, DeviceItemDisplay>> {

        // get known Bluetooth devices
        val kbs = DevicesDb.of(getApplication()).db.knownBluetoothDao().getAll().map {
            it to DeviceItemDisplay(it, it.name, it.mac, iconBt, DeviceItemDisplay.State.KNOWN)
        }

        // set CONNECTED state in known Bluetooth devices if needed
        val cdi = currentDeviceInfo()

        if (cdi is BluetoothDevice) {
            kbs.forEach { knownDevice ->
                if (knownDevice.first.mac.equals(cdi.address, true)) {
                    knownDevice.second.state =
                        if (isConnected()) DeviceItemDisplay.State.CONNECTED else DeviceItemDisplay.State.ACTIVE

                    if (isConnected()) {
                        knownDevice.second.title = "CONNECTED to " + knownDevice.second.title
                    }

                }
            }
        }

        // Usb devices
        val usbs = deviceCache.filter { it.first is UsbDevice }.map {
            val u = it.first as UsbDevice
            val display = DeviceItemDisplay(
                u, u.deviceName, "%04X:%04X\n%s".format(
                    u.vendorId, u.productId,
                    UManager.of(getApplication()).info(
                        u.vendorId, u.productId
                    )
                ), iconUsb, DeviceItemDisplay.State.PAIRED
            )

            if (isConnected() && cdi is UsbDevice) {
                display.state = DeviceItemDisplay.State.CONNECTED
                display.title = "CONNECTED to " + display.title
            }
            it.first to display
        }


        // return combined list and remove duplicates in scans
        return listOf(
            usbs.toList(),
            kbs.toList(),
            deviceCache.toList()
        ).flatten().distinctBy {
            val item = it.first
            when (item) {
                is KnownBluetooth -> item.mac
                is BluetoothDevice -> item.address
                else -> ""
            }
        }

    }

    fun currentDeviceInfo() = if (UManager.of(getApplication()).hasDevice()) {
        UManager.of(getApplication()).currentDevice()
    } else {
        BtManager.of(getApplication()).currentDevice()
    }

    fun isConnected(): Boolean = if (UManager.of(getApplication()).hasDevice()) {
        UManager.of(getApplication()).isConnected()
    } else if (BtManager.of(getApplication()).hasDevice()) {
        BtManager.of(getApplication()).isConnected()
    } else false

    val pairingReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intentArg: Intent?) {
            val intent = intentArg ?: return
            val state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, -1)
            Timber.d("action: %s", intent.action)
            Timber.d("bond state: %d", state)

            when (state) {
                BluetoothDevice.BOND_BONDING -> return
                BluetoothDevice.BOND_BONDED -> {
                    deviceToConnect?.let(::connect)
                }
                else -> {
                    deviceConnectedResult.postValue(null)
                    deviceToConnect = null
                }
            }

            pairing = false
            (getApplication() as App).unregisterReceiver(this)

        }

    }
}