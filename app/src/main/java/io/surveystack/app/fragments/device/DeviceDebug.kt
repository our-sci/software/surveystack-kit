package io.surveystack.app.fragments.device

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import io.surveystack.app.R
import io.surveystack.app.app
import io.surveystack.app.arch.ConsumableObserver
import io.surveystack.app.databinding.DeviceDebugFragmentBinding

class DeviceDebug : Fragment() {

    lateinit var binding: DeviceDebugFragmentBinding

    companion object {
        fun newInstance() = DeviceDebug()
    }

    private lateinit var viewModel: DeviceDebugViewModel


    private val refreshObserver = ConsumableObserver<Void> { t: Void?, consumed: Boolean ->
        if (consumed) {
            return@ConsumableObserver
        }

        binding.textView.text = ""
        binding.textView.append(viewModel.data)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(DeviceDebugViewModel::class.java)

        viewModel.refreshResult.observe(this, refreshObserver)
        viewModel.refreshLog()
        viewModel.registerCallbacks()

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup??,
        savedInstanceState: Bundle?
    ): View? {
        binding = DeviceDebugFragmentBinding.inflate(inflater, container, false)
        binding.btRefresh.setOnClickListener {
            viewModel.refreshLog()
        }

        binding.tvSend.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                val ending = when (binding.grpLineEnding.checkedRadioButtonId) {
                    R.id.radio_none -> ""
                    R.id.radio_n -> "\n"
                    R.id.radio_rn -> "\r\n"
                    else -> ""
                }
                viewModel.sendData("%s%s".format(v.text.toString(), ending))
                v.text = ""
                v.invalidate()
                false
            } else true
        }
        binding.textView.movementMethod = ScrollingMovementMethod()
        binding.textView.setOnLongClickListener {
            val clipboard = app().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.setPrimaryClip(
                ClipData.newPlainText(
                    "debug output",
                    viewModel.data.toString()
                )
            )
            Toast.makeText(app(), "Copied to Clipboard", Toast.LENGTH_SHORT).show()
            true
        }
        return binding.root
    }

}