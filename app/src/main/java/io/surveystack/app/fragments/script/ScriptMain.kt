package io.surveystack.app.fragments.script

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.net.Uri
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.webkit.WebViewAssetLoader
import androidx.webkit.WebViewClientCompat
import io.surveystack.app.MainActivity
import io.surveystack.app.R
import io.surveystack.app.app
import io.surveystack.app.arch.ConsumableObserver
import io.surveystack.app.databinding.ScriptMainFragmentBinding
import io.surveystack.app.device.bt.BtManager
import io.surveystack.app.device.usb.UManager
import io.surveystack.app.fragments.HomeViewModel
import io.surveystack.app.measurement.MeasurementLoader
import org.oursci.android.common.measurement.MeasurementScript
import timber.log.Timber

class ScriptMain : Fragment() {


    private lateinit var viewModel: ScriptMainViewModel
    private lateinit var mainViewModel: HomeViewModel

    private lateinit var controlBinding: ScriptMainFragmentBinding
    private var scriptId: String = ""


    val args: ScriptMainArgs by navArgs()

    private val stateObserver = ConsumableObserver<ScriptDisplay.State> { value, consumed ->
        if (consumed || value == null) {
            return@ConsumableObserver
        }

        controlBinding.control?.state = value
        controlBinding.invalidateAll()
    }


    private val assetLoader by lazy {
        WebViewAssetLoader.Builder()
            .setDomain("surveystack.local")
            .addPathHandler(
                "/files/",
                WebViewAssetLoader.InternalStoragePathHandler(
                    requireContext(),
                    MeasurementLoader.of(app()).measurementDir
                )
            )
            .addPathHandler("/assets/", WebViewAssetLoader.AssetsPathHandler(requireContext()))
            .build()
    }

    private class LocalContentWebViewClient(private val assetLoader: WebViewAssetLoader) :
        WebViewClientCompat() {
        @RequiresApi(21)
        override fun shouldInterceptRequest(
            view: WebView,
            request: WebResourceRequest
        ): WebResourceResponse? {
            Timber.d("intercept ${request.url}")
            return assetLoader.shouldInterceptRequest(request.url)
        }

        // to support API < 21
        override fun shouldInterceptRequest(
            view: WebView,
            url: String
        ): WebResourceResponse? {
            Timber.d("intercept ${url}")
            return assetLoader.shouldInterceptRequest(Uri.parse(url))
        }
    }


    private val scriptObserver = ConsumableObserver<MeasurementScript> { value, consumed ->


        if (consumed || value == null) {
            return@ConsumableObserver
        }
        viewModel.currentMeasurementScript = value


        if (viewModel.requireDevice() && !viewModel.isConnected()) {
            controlBinding.btMeasurement.text = "Connect to device"
            controlBinding.control = ScriptDisplay(
                "Connect to device",
                null
            )
        } else {
            controlBinding.btMeasurement.text = "Start Measurement"
            controlBinding.control = ScriptDisplay(
                "Start Measurement",
                viewModel.currentMeasurementScript?.manifestCreated
            )
            controlBinding.invalidateAll()
            if (viewModel.autorun) {
                startMeasurement()
            }

        }


    }


    private val cookieObserver = ConsumableObserver<Pair<String, String>> { value, consumed ->
        if (consumed || value == null) {
            return@ConsumableObserver
        }

        val cookieManager = CookieManager.getInstance()
        cookieManager.setCookie(value.first, value.second)
    }

    private val sensorProgressObserver = ConsumableObserver<Float> { value, consumed ->

        Timber.d("updating progessbar with value: %d", value?.toInt() ?: 0)
        controlBinding.progressMeasurement.progress = value?.toInt() ?: 0
        controlBinding.progressMeasurement.invalidate()
    }


    private val sensorResultObserver = ConsumableObserver<Void> { value, consumed ->
        if (consumed) {
            return@ConsumableObserver
        }

        Timber.d("sensorResultObserver")
        // return@ConsumableObserver
        controlBinding.webviewMeasurementResult.loadUrl("about:blank")

        val scriptId = arguments?.getString("id") ?: ""


        // TODO what if we get the result? Navigate to where?
        findNavController().navigate(
            R.id.action_scriptMain_to_scriptResult, bundleOf(
                "id" to scriptId
            )
        )
    }


    private val onDataAvailableObserver = ConsumableObserver<Void> { _, consumed ->
        if (consumed) {
            return@ConsumableObserver
        }

        controlBinding.webviewMeasurementResult.loadUrl("javascript:serial.onDataAvailable()")
    }


    private val webAnswerObserver = ConsumableObserver<String> { str: String?, consumed: Boolean ->
        if (consumed || str == null) {
            return@ConsumableObserver
        }

        viewModel.webInputData.append(str)
        controlBinding.webviewMeasurementResult.loadUrl("javascript:serial.onWebInputAvailable()")
        controlBinding.invalidateAll()
    }

    private val deviceConnectedResultObserver = ConsumableObserver<String> { status, consumed ->
        if (consumed) {
            return@ConsumableObserver
        }

        when (status) {
            "CONNECTED" -> {
                Timber.d("measurement fragment.. CONNECTED")
                controlBinding.control?.state = ScriptDisplay.State.IDLE
                controlBinding.btMeasurement.text =
                    viewModel.currentMeasurementScript?.actionName
                        ?: "Start Measurement"

                startMeasurement()

            }
            "CONNECTING" -> {
                Timber.d("measurement fragment.. CONNECTING")
                controlBinding.tvConnecting.text =
                    "Connecting to '${viewModel.deviceName()}'"
                controlBinding.control?.state = ScriptDisplay.State.CONNECTING
            }
            "NONE" -> {
                Timber.d("measurement fragment.. NONE")
                controlBinding.tvConnectingFailed.text =
                    "Could not connect to '${viewModel.deviceName()}'\n\nCheck if device is on.\nReset device and try again."
                controlBinding.control?.state = ScriptDisplay.State.CONNECTING_FAILED
            }
        }

        controlBinding.invalidateAll()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        Timber.d("onCreateView")
        viewModel = ViewModelProvider(requireActivity()).get(ScriptMainViewModel::class.java)
        mainViewModel = ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)

        viewModel.clearResult()
        val scriptId = arguments?.getString("id") ?: ""
        this.scriptId = scriptId


        Timber.d("script id: ${scriptId}")

        val cookieManager = CookieManager.getInstance()
        cookieManager.setAcceptCookie(true)

        controlBinding = ScriptMainFragmentBinding.inflate(inflater, container, false)

        if (0 != app().applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE) {
            WebView.setWebContentsDebuggingEnabled(true)
        }

        controlBinding.webviewMeasurementResult.clearCache(true)
        cookieManager.setAcceptThirdPartyCookies(controlBinding.webviewMeasurementResult, true)

        controlBinding.webviewMeasurementResult.settings.apply {
            cacheMode = WebSettings.LOAD_NO_CACHE
            javaScriptEnabled = true
            allowContentAccess = false
            allowFileAccess = false
            allowUniversalAccessFromFileURLs = false
            allowFileAccessFromFileURLs = false

        }


        controlBinding.webviewMeasurementResult.apply {
            addJavascriptInterface(viewModel.applicationInterface, "android")
            addJavascriptInterface(viewModel.sensorInterface, "sensor")
            addJavascriptInterface(viewModel.processorInterface, "processor")
        }

        controlBinding.progressMeasurement.progress = 0

        controlBinding.webviewMeasurementResult.loadUrl("about:blank")

        controlBinding.webviewMeasurementResult.webViewClient =
            LocalContentWebViewClient(assetLoader)

        controlBinding.textView.movementMethod = ScrollingMovementMethod()



        controlBinding.btMeasurement.setOnClickListener {
            if (viewModel.requireDevice() && !viewModel.isConnected()) {
                //Toast.makeText(activity, "Not Connected", Toast.LENGTH_SHORT).show()
                if (UManager.of(app()).hasDevice()) {
                    viewModel.connectToLastDevice()
                } else if (BtManager.of(app()).hasDevice()) {
                    val adapter = BluetoothAdapter.getDefaultAdapter()
                    if (adapter.state == BluetoothAdapter.STATE_OFF) {
                        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                        requireActivity().startActivityForResult(
                            enableBtIntent,
                            MainActivity.RC_BLUETOOTH_ENABLE_FROM_MEASUREMENT
                        )
                        return@setOnClickListener
                    }
                    viewModel.connectToLastDevice()
                } else {
                    val action = ScriptMainDirections.actionScriptMainToNavigationDevice(true)
                    findNavController().navigate(action)
                }

                return@setOnClickListener
            }


            startMeasurement()
        }


        controlBinding.btCancel.setOnClickListener {
            controlBinding.control?.state = ScriptDisplay.State.IDLE
            controlBinding.invalidateAll()
            controlBinding.webviewMeasurementResult.loadUrl("about:blank")
        }

        controlBinding.btConnectList.setOnClickListener {
            findNavController().navigate(R.id.action_scriptMain_to_navigation_device)
        }

        controlBinding.btConnectRetry.setOnClickListener {
            viewModel.connectToLastDevice()
        }

        controlBinding.btCancelConnecting.setOnClickListener {
            viewModel.cancelConnect()
        }


        viewModel.sensorProgressResult.observe(this, sensorProgressObserver)
        viewModel.sensorResult.observe(this, sensorResultObserver)
        viewModel.sensorDataAvailable.observe(this, onDataAvailableObserver)
        viewModel.cookieResult.observe(this, cookieObserver)

        mainViewModel.downLoadResult.observe(this, scriptObserver)
        mainViewModel.stateResult.observe(this, stateObserver)

        app().webAnswerData.observe(this, webAnswerObserver)
        viewModel.deviceConnectedResult.observe(this, deviceConnectedResultObserver)


        controlBinding.btMeasurement.text = "Run Measurement"

        controlBinding.invalidateAll()

        return controlBinding.root
    }

    private fun startMeasurement() {
        viewModel.clearResult()
        viewModel.clearBuffer()

        controlBinding.progressMeasurement.progress = 0

        controlBinding.control?.state = ScriptDisplay.State.MEASUREMENT
        controlBinding.invalidateAll()


        controlBinding.root.visibility = View.VISIBLE

        // controlBinding.webviewMeasurementResult.loadUrl("file:///android_asset/sensor/sensor.html")
        controlBinding.webviewMeasurementResult.loadUrl("https://surveystack.local/assets/sensor/sensor.html")

        viewModel.autorun = false
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Timber.d("calling load")
        load()
    }


    fun load() {
        controlBinding.control = ScriptDisplay(
            "Start Measurement",
            viewModel.currentMeasurementScript?.manifestCreated
        )

        viewModel.setupCallbacks()
        mainViewModel.load(scriptId)
    }


}