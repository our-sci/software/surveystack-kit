package io.surveystack.app.fragments.device

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.surveystack.app.R

class DeviceScan : Fragment() {

    companion object {
        fun newInstance() = DeviceScan()
    }

    private lateinit var viewModel: DeviceScanViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.device_scan_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DeviceScanViewModel::class.java)
        // TODO: Use the ViewModel
    }

}