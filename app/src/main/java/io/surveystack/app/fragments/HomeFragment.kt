package io.surveystack.app.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import io.surveystack.app.R
import io.surveystack.app.arch.ConsumableObserver
import io.surveystack.app.databinding.HomeFragmentBinding
import io.surveystack.app.repository.Repository
import org.oursci.android.common.measurement.MeasurementScript
import org.oursci.android.common.measurement.ScriptBase


class HomeFragment : Fragment() {


    val adapter = ScriptItemRecyclerViewAdapter {
        when (it) {
            // is Repository.RepoItem -> viewModel.fetchScript(it)
            is Repository.RepoItem -> findNavController().navigate(
                R.id.action_navigation_home_to_scriptMain,
                bundleOf("id" to it.name.dropLast(4)) // .zip
            )
            else -> findNavController().navigate(
                R.id.action_navigation_home_to_scriptMain, bundleOf("id" to it.id)
            )
        }
    }

    companion object {
        fun newInstance() = HomeFragment()
    }


    private val listObserver = ConsumableObserver<Array<ScriptBase>> { t, consumed ->
        if (consumed || t == null) {
            return@ConsumableObserver
        }

        adapter.values.clear()
        adapter.values.addAll(t)
        adapter.notifyDataSetChanged()
    }


    private val downloadObserver =
        ConsumableObserver<MeasurementScript> { measurementScript, consumed ->
            if (consumed) {
                return@ConsumableObserver
            }

            val script = measurementScript ?: return@ConsumableObserver handleDownloadFailed()

            findNavController().navigate(
                R.id.action_navigation_home_to_scriptMain, bundleOf("id" to script.id)
            )
        }


    fun handleDownloadFailed() {
        // TODO what if we don't have a script?
    }

    private lateinit var viewModel: HomeViewModel
    lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = HomeFragmentBinding.inflate(inflater, container, false)

        binding.list.adapter = adapter

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        viewModel.listResult.observe(this, listObserver)
        viewModel.downLoadResult.observe(this, downloadObserver)
        binding.btTest.setOnClickListener {
            requireActivity().finish()
        }

        /*
        binding.btStart.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://app.surveystack.io"))
            startActivity(browserIntent)
        }
        */

        // viewModel.downloadAll()
        viewModel.fetchRepo()
    }
}
