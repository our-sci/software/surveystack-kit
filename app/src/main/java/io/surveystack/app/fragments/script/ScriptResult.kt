package io.surveystack.app.fragments.script

import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.webkit.WebViewAssetLoader
import androidx.webkit.WebViewClientCompat
import io.surveystack.app.app
import io.surveystack.app.arch.ConsumableObserver
import io.surveystack.app.databinding.ScriptResultFragmentBinding
import io.surveystack.app.html
import io.surveystack.app.measurement.MeasurementLoader
import timber.log.Timber

class ScriptResult : Fragment() {

    lateinit var viewModel: ScriptMainViewModel


    private val assetLoader by lazy {
        WebViewAssetLoader.Builder()
            .setDomain("surveystack.local")
            .addPathHandler(
                "/files/",
                WebViewAssetLoader.InternalStoragePathHandler(
                    requireContext(),
                    MeasurementLoader.of(app()).measurementDir
                )
            )
            .addPathHandler("/assets/", WebViewAssetLoader.AssetsPathHandler(requireContext()))
            .build()
    }

    private class LocalContentWebViewClient(private val assetLoader: WebViewAssetLoader) :
        WebViewClientCompat() {
        @RequiresApi(21)
        override fun shouldInterceptRequest(
            view: WebView,
            request: WebResourceRequest
        ): WebResourceResponse? {
            Timber.d("intercept ${request.url}" )
            return assetLoader.shouldInterceptRequest(request.url)
        }

        // to support API < 21
        override fun shouldInterceptRequest(
            view: WebView,
            url: String
        ): WebResourceResponse? {
            Timber.d("intercept ${url}" )
            return assetLoader.shouldInterceptRequest(Uri.parse(url))
        }
    }

    private lateinit var controlBinding: ScriptResultFragmentBinding
    private var measurementId: String = ""

    private val autoRunObserver = ConsumableObserver<String> { t: String?, consumed: Boolean ->
        if (consumed) {
            return@ConsumableObserver
        }
    }

    private val saveRequiredObserver = ConsumableObserver<Void> { t: Void?, consumed: Boolean ->
        if (consumed) {
            return@ConsumableObserver
        }
    }

    private val storeMeasurementResultObserver = ConsumableObserver<Boolean> { success, consumed ->
        if (consumed || success == null) return@ConsumableObserver

        if (success) {
            // TODO navigate to result list
        } else {
            Toast.makeText(activity, "error storing result", Toast.LENGTH_SHORT).show()
        }

    }

    private val resultLoadedObserver = ConsumableObserver<Void> { _, consumed ->
        if (consumed) return@ConsumableObserver
        controlBinding.webviewMeasurementResult.loadUrl("file:///android_asset/processor/processor.html")
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(requireActivity()).get(ScriptMainViewModel::class.java)

        measurementId = arguments?.getString("id", "") ?: ""
        // viewModel.clearResult()

        // TODO load measurement

        controlBinding = ScriptResultFragmentBinding.inflate(inflater, container, false)
        controlBinding.btFinish.visibility = View.VISIBLE


        controlBinding.webviewMeasurementResult.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                // TODO hide progress bar here
                super.onPageFinished(view, url)
            }
        }


        controlBinding.webviewMeasurementResult.webViewClient = LocalContentWebViewClient(assetLoader)

        controlBinding.textView.movementMethod = ScrollingMovementMethod()

        controlBinding.webviewMeasurementResult.settings.setAppCacheEnabled(false)
        controlBinding.webviewMeasurementResult.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        controlBinding.webviewMeasurementResult.clearCache(true)


        controlBinding.webviewMeasurementResult.settings.apply {
            cacheMode = WebSettings.LOAD_NO_CACHE
            javaScriptEnabled = true
            allowContentAccess = false
            allowFileAccess = false
            allowUniversalAccessFromFileURLs = false
            allowFileAccessFromFileURLs = false

        }


        controlBinding.webviewMeasurementResult.apply {
            addJavascriptInterface(viewModel.applicationInterface, "android")
            addJavascriptInterface(viewModel.sensorInterface, "sensor")
            addJavascriptInterface(viewModel.processorInterface, "processor")
        }


//        controlBinding.btRedo.visibility = if (filePath.isEmpty()) View.VISIBLE else View.GONE
//        controlBinding.btSave.visibility = if (filePath.isEmpty()) View.VISIBLE else View.GONE


        viewModel.apply {
            resultLoaded.observe(this@ScriptResult, resultLoadedObserver)
            storeSuccessResult.observe(
                this@ScriptResult,
                storeMeasurementResultObserver
            )
            saveRequired.observe(this@ScriptResult, saveRequiredObserver)
            autoRunResult.observe(this@ScriptResult, autoRunObserver)
        }



        controlBinding.webviewMeasurementResult.loadUrl("https://surveystack.local/assets/processor/processor.html")

        // todo add redo button?


        controlBinding.btRedo.setOnClickListener {
            viewModel.autorun = true
            findNavController().popBackStack()
        }

        controlBinding.btFinish.setOnClickListener {
            viewModel.saveResult {
                requireActivity().finish()
            }
        }

        return controlBinding.root
    }


}