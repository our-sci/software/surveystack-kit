package io.surveystack.app.fragments

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import io.surveystack.app.arch.ConsumableLiveData
import io.surveystack.app.base.AppViewModel
import io.surveystack.app.fragments.script.ScriptDisplay
import io.surveystack.app.repository.Repository
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import io.surveystack.app.measurement.MeasurementLoader
import org.oursci.android.common.measurement.MeasurementScript
import org.oursci.android.common.measurement.ScriptBase
import timber.log.Timber
import java.io.File
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

class HomeViewModel(application: Application) : AppViewModel(application) {

    val listResult = ConsumableLiveData<Array<ScriptBase>>()
    val zipFiles = mutableListOf<Repository.RepoItem>()
    val downLoadResult = ConsumableLiveData<MeasurementScript>()
    val stateResult = ConsumableLiveData<ScriptDisplay.State>()


    val repo by lazy {
        Repository.of(app)
    }


    val client = OkHttpClient()

    data class ScriptInfo(val name: String, val url: String, val id: String)

    private fun download(fileUrl: String): MeasurementScript? {

        Timber.d("running download for $fileUrl")

        try {

            val request = Request.Builder()
                .url(fileUrl).build()
            val inputStream =
                client.newCall(request).execute().body()?.byteStream() ?: throw Exception(
                    "unable to fetch gitlab tree"
                )

            val outDir = File(app.cacheDir, "scripts_download")
            outDir.mkdirs()
            val outFile = File(outDir, "download.zip")
            outFile.delete()
            outFile.deleteOnExit()

            outFile.outputStream().use { out ->
                inputStream.buffered().use {
                    it.copyTo(out)
                }
            }

            Timber.d("exracting script")
            val script = MeasurementLoader.of(app).extract(outFile, Date().time)
            Timber.d("loaded ${script?.name}")
            return script
        } catch (e: Exception) {
            Timber.e(e)
        }

        return null
    }

    private fun downloadRunnable(fileUrl: String): Runnable = Runnable {
        download(fileUrl)
    }

    fun downloadAll() {
        thread {

            try {
                val allScripts = app.assets.open("scripts.json").use {
                    it.readBytes().toString(Charsets.UTF_8)
                }

                val json = JSONArray(allScripts)


                val scripts = mutableListOf<ScriptInfo>()
                val resultingScripts = mutableListOf<MeasurementScript>()
                for (i in 0 until json.length()) {
                    val item = json.getJSONObject(i)
                    Timber.d("item: ${item.getString("name")}")
                    scripts.add(
                        ScriptInfo(
                            item.getString("name"),
                            item.getString("url"),
                            item.getString("id")
                        )
                    )
                }

                scripts.forEach {
                    download(it.url)?.let { r -> resultingScripts.add(r) }
                }
                listResult.postValue(resultingScripts.toTypedArray())
            } catch (e: Exception) {
                listResult.postValue(null)
            }
        }

    }

    private fun fetchScriptUnsafeSync(repoItem: Repository.RepoItem): MeasurementScript? {
        return download("${repoItem.url}/raw")
    }


    private fun fetchScriptSync(repoItem: Repository.RepoItem): MeasurementScript? {
        return try {
            val script = download("${repoItem.url}/raw")
                ?: throw Exception("unable to download script: ${repoItem.url}")
            script
        } catch (e: Exception) {
            Timber.e(e)
            val script = MeasurementLoader.of(app).getAllMeasurementScripts().find {
                "${it.id}.zip" == repoItem.name
            }
            script
        }
    }

    fun fetchScript(repoItem: Repository.RepoItem) {
        thread {
            val script = fetchScriptSync(repoItem)
            downLoadResult.postValue(script)
        }
    }

    private fun cachedSha(item: Repository.RepoItem): String {
        return app.getSharedPreferences("default", Context.MODE_PRIVATE)
            .getString("sha-${item.path}", "") ?: ""
    }

    private fun updateSha(item: Repository.RepoItem) {
        app.getSharedPreferences("default", Context.MODE_PRIVATE)?.edit()
            ?.putString("sha-${item.path}", item.sha256)?.apply()
    }


    private fun fetchRepoSync() {
        Timber.d("fetching repo")
        zipFiles.clear()
        zipFiles.addAll(repo.fetch("24369909").filter { it.name.endsWith(".zip") })
        zipFiles.forEach {
            Timber.d("found zip ${it.path}, ${it.sha256}")
            if (cachedSha(it) != it.sha256) {
                // download
                Timber.d("script outdated, fetching: ${it.path}")

                try {
                    val script = fetchScriptUnsafeSync(it)
                    if (script != null) {
                        updateSha(it)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            it.path
            // precache
        }
    }

    data class CommitHashResult(val value: String, val stale: Boolean = false)

    private fun fetchCommitHash(): CommitHashResult {
        val hash = repo.fetchCurrentCommitHash("24369909")
        val cached = app.getSharedPreferences("default", Context.MODE_PRIVATE)
            ?.getString("current-repo-commit-hash", "") ?: ""

        Timber.d("cached SHA: $cached")

        return CommitHashResult(
            stale = if(cached == "") true else hash != cached,
            value = hash
        )
    }

    private fun updateCommitHash(hash: String) {
        app.getSharedPreferences("default", Context.MODE_PRIVATE)?.edit()
            ?.putString("current-repo-commit-hash", hash)?.apply()
    }


    fun fetchRepo() {
        thread {
            try {
                val commitHashResult = fetchCommitHash()
                Timber.d("commit hash: $commitHashResult")
                if (!commitHashResult.stale) {
                    // we are up to date, skip download, default to downloaded scripts
                    val items =
                        (MeasurementLoader.of(app).getAllMeasurementScripts()).map { it }

                    listResult.postValue(items.toTypedArray())
                    return@thread
                }

                fetchRepoSync()
                updateCommitHash(commitHashResult.value)

                listResult.postValue(zipFiles.toTypedArray())

            } catch (e: Exception) {
                val items =
                    (MeasurementLoader.of(app).getAllMeasurementScripts()).map { it }

                listResult.postValue(items.toTypedArray())
            }
        }
    }

    val loading = AtomicBoolean(false)

    fun load(scriptId: String) {

        if (loading.getAndSet(true)) {
            return
        }


        thread {

            try {
                stateResult.postValue(ScriptDisplay.State.DOWNLOADING)

                val commitHashResult = fetchCommitHash()
                Timber.d("commit hash: $commitHashResult")

                if (commitHashResult.stale) {
                    fetchRepoSync()
                    updateCommitHash(commitHashResult.value)

                    val res = zipFiles.find {
                        it.name == if (scriptId.endsWith(".zip")) it else "$scriptId.zip"
                    }

                    if (res != null) {
                        val script = fetchScriptSync(res)
                        if (script != null) {
                            downLoadResult.postValue(script)
                            return@thread
                        }
                    }
                }

            } catch (e: Exception) {
                Timber.e(e) // offline
            }
            loading.set(false)

            stateResult.postValue(ScriptDisplay.State.IDLE)

            // load from cache
            val script = MeasurementLoader.of(app).fromId(scriptId)
            downLoadResult.postValue(script)
        }
    }
}