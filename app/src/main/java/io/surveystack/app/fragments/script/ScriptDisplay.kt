package io.surveystack.app.fragments.script

import android.view.View
import androidx.annotation.UiThread

class ScriptDisplay(
    val actionName: String,
    val created: Long?
) {

    enum class State {
        IDLE,
        MEASUREMENT,
        DEBUG,
        CONNECTING,
        CONNECTING_FAILED,
        DOWNLOADING
    }

    var state = State.IDLE
        @UiThread
        set(value) {
            field = value
        }

    var progressText: String = ""
    var progress: Int = 0

    fun visibilityStartButton() = when (state) {
        State.IDLE -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityProgressBar() = when (state) {
        State.MEASUREMENT -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityText() = when (state) {
        State.DEBUG -> View.GONE
        else -> View.VISIBLE
    }

    fun visibilityWebView() = View.GONE

    fun visibilityDebug() = when (state) {
        State.DEBUG -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityConnecting() = when (state) {
        State.CONNECTING -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityDownloading() = when (state) {
        State.DOWNLOADING -> View.VISIBLE
        else -> View.GONE
    }


    fun visibilityConnectingFailed() = when (state) {
        State.CONNECTING_FAILED -> View.VISIBLE
        else -> View.GONE
    }

    fun textTitle(): String {
        return ""
    }

    fun hint(): String = when (state) {
        State.MEASUREMENT -> "Running"
        else -> ""
    }


}