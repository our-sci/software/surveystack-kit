package io.surveystack.app.fragments.device

import android.app.Application
import android.text.Spanned
import io.surveystack.app.arch.ConsumableLiveData
import io.surveystack.app.base.AppViewModel
import io.surveystack.app.device.bt.BtManager
import io.surveystack.app.device.usb.UManager
import io.surveystack.app.html
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

class DeviceDebugViewModel(application: Application) : AppViewModel(application) {

    val refreshResult = ConsumableLiveData<Void>()
    private val refreshing = AtomicBoolean(false)

    var data: Spanned = html("")

    fun refreshLog() {
        if (refreshing.get()) {
            return
        }

        refreshing.set(true)
        thread {
            data = html(
                if (BtManager.of(app).hasDevice()) {
                    BtManager.of(app).getInterceptedData()
                } else {
                    UManager.of(app).getInterceptedData()
                }
            )
            refreshResult.postValue(null)
            refreshing.set(false)
        }
    }

    var dataBuffer = StringBuffer(1000)
    val dataLock = Object()


    fun isConnected(): Boolean = if (BtManager.of(app).hasDevice()) {
        BtManager.of(app).isConnected()
    } else {
        UManager.of(app).isConnected()
    }


    fun registerCallbacks() {
        BtManager.of(app).registerCallback(
            onScan = {},
            onStatusChange = {},
            onDataRead = { data ->
                synchronized(dataLock) {
                    dataBuffer.append(data)
                    onNewData()
                }
            })
        UManager.of(app).registerCallback({}, {}, { data ->
            synchronized(dataLock) {
                dataBuffer.append(data)
                onNewData()
            }
        })
    }

    private fun onNewData() {
        try {
            refreshLog()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun sendData(data: String) {
        if (BtManager.of(app).connected.get()) {
            BtManager.of(app).write(data)
        } else if (UManager.of(app).connected.get()) {
            UManager.of(app).write(data)
        }
    }
}