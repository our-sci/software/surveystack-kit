package io.surveystack.app.fragments.device


import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.DialogInterface
import android.content.Intent
import android.hardware.usb.UsbDevice
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import io.surveystack.app.MainActivity
import io.surveystack.app.R
import io.surveystack.app.arch.ConsumableObserver
import io.surveystack.app.base.App
import io.surveystack.app.databinding.DeviceListFragmentBinding
import io.surveystack.app.device.persist.KnownBluetooth
import io.surveystack.app.html
import timber.log.Timber

@SuppressLint("MissingPermission")
class DeviceList : Fragment() {

    lateinit var binding: DeviceListFragmentBinding

    companion object {
        fun newInstance() = DeviceList()
    }

    private lateinit var viewModel: DeviceListViewModel
    lateinit var app: App

    val registerForResult = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val intent = result.data
        }
    }

    val bluetoothManager by lazy {
        ContextCompat.getSystemService(requireContext(), BluetoothManager::class.java)
    }

    val args: DeviceListArgs by navArgs()

    val adapter by lazy {
        DeviceListAdapter({ view, device ->

            val pressedButton = when (view.id) {
                R.id.bt_disconnect -> "Disconnect"
                R.id.bt_connect -> "Connect"
                R.id.bt_pair -> "Pair"
                R.id.bt_forget -> "Forget"
                else -> "???"
            }

            Timber.d("Pressed button: $pressedButton")

            if (view.id == R.id.bt_connect || view.id == R.id.bt_pair) {
                when (device) {
                    is BluetoothDevice, is KnownBluetooth -> {

                        val adapter = bluetoothManager?.adapter ?: return@DeviceListAdapter
                        if (adapter.state == BluetoothAdapter.STATE_OFF) {

                            if (!adapter.isEnabled) {
                                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                                registerForResult.launch(enableBtIntent)
                            }

                            return@DeviceListAdapter
                        }

                        val bluetoothDevice = when (device) {
                            is BluetoothDevice -> device
                            is KnownBluetooth -> adapter.getRemoteDevice(device.mac)
                            else -> null
                        } ?: return@DeviceListAdapter

                        Timber.d("clicked on device %s", bluetoothDevice.name)

                        if (viewModel.connect(bluetoothDevice)) {
                            binding.display?.state = DeviceListDisplay.State.CONNECTING
                            binding.invalidateAll()
                        } else {
                            AlertDialog.Builder(requireActivity())
                                .setTitle("Pairing Device")
                                .setMessage(
                                    html(
                                        "In order to connect to the device, please use the system dialog " +
                                                "to create a pairing first.<br><br>The default pairing code is <b>1234</b>."
                                    )
                                )
                                .setIcon(R.drawable.ic_reflectometer_simple_primary)
                                .setPositiveButton("Start Pairing") { dialogInterface: DialogInterface?, _: Int ->
                                    binding.invalidateAll()
                                    viewModel.bond(bluetoothDevice)
                                    binding.display?.state = DeviceListDisplay.State.PAIRING
                                    // startActivity(Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS))
                                    dialogInterface?.dismiss()
                                }
                                .setNegativeButton("Cancel") { dialogInterface, _ ->
                                    dialogInterface?.dismiss()
                                }.create().show()
                        }


                    }
                    is UsbDevice -> {
                        viewModel.connect(device)
                        binding.display?.state = DeviceListDisplay.State.CONNECTING
                        binding.invalidateAll()
                    }
                    else -> {
                        Timber.e("unknown device type")
                    }
                }
            } else if (view.id == R.id.bt_disconnect) {
                when (device) {
                    is UsbDevice -> viewModel.disconnectUSB()
                    else -> viewModel.disconnect()
                }
            } else if (view.id == R.id.bt_forget) {
                when (device) {
                    is KnownBluetooth -> viewModel.removeKnownBluetoothDevice(device)
                }
            } else if (view.id == R.id.bt_more) {
                Timber.d("deviceInfoFromDeviceList")
                // TODO navigate to info screen
                findNavController().navigate(R.id.action_navigation_device_to_deviceSettings)

            }

        }, requireActivity() as MainActivity)
    }


    private val deviceListResultObserver =
        ConsumableObserver<List<Pair<Any, DeviceItemDisplay>>> { list, consumed ->
            if (!consumed && list != null) {
                Timber.d("updating device list")

                adapter.items.clear()
                adapter.items.addAll(list)
                adapter.notifyDataSetChanged()
            }
        }

    private val deviceConnectedResultObserver = ConsumableObserver<String> { device, consumed ->
        if (!consumed && device != null) {
            Timber.d("connected to device ${device}")
            // app().navigationManager.deviceInfoFromDeviceList()
            binding.display?.state = DeviceListDisplay.State.IDLE
        } else if (device == null) {
            binding.display?.state = DeviceListDisplay.State.IDLE
            //toast("Could not connect to device")
        }

        binding.invalidateAll()
    }


    private val deviceScanDoneObserver = ConsumableObserver<Void> { _, consumed ->

        if (binding.display?.state == DeviceListDisplay.State.CONNECTING) {
            return@ConsumableObserver
        }

        if (!consumed) {
            binding.display?.state = when {
                viewModel.isPairing() -> DeviceListDisplay.State.PAIRING
                else -> DeviceListDisplay.State.SCAN_DONE
            }

            binding.invalidateAll()
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        app = requireContext().applicationContext as App
        viewModel = ViewModelProvider(this).get(DeviceListViewModel::class.java)

        //viewModel.cancelConnect()

        binding.btScan.setOnClickListener {

            if ((requireActivity() as MainActivity).requiresPermission()) {
                (requireActivity() as MainActivity).showPermissionDialog(
                    "App requires Permmission",
                    "In order to scan, acquire and process data, a couple of " +
                            "permissions are required."
                )
                return@setOnClickListener
            }

            binding.display?.state = DeviceListDisplay.State.SCANNING
            binding.invalidateAll()

            val adapter = BluetoothAdapter.getDefaultAdapter()
            if (adapter.state == BluetoothAdapter.STATE_OFF) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                registerForResult.launch(enableBtIntent)
                return@setOnClickListener
            }

            viewModel.startDeviceScan()
        }


        binding.btCancel.setOnClickListener {
            binding.display?.state = DeviceListDisplay.State.IDLE
            binding.invalidateAll()
            viewModel.cancelConnect()
        }

        binding.btDeviceMissing.setOnClickListener {
            AlertDialog.Builder(requireActivity())
                .setTitle("Missing Bluetooth Device")
                .setMessage(html("Please make sure your device is turned on. A green light means the device is switched on. Press the small button next to the USB connector to switch the device on."))
                .setPositiveButton("OK") { dialog, _ ->
                    dialog.dismiss()
                }.create().show()
            Timber.d("showing dialog")
        }

        binding.btStop.setOnClickListener {
            Timber.d("Clicked on stop scan")
            viewModel.stopDeviceScan()
        }

        binding.btBack.setOnClickListener {
            Timber.d("pressing back button")

            if (viewModel.isConnecting()) {
                binding.display?.state = DeviceListDisplay.State.IDLE
                binding.invalidateAll()
                viewModel.cancelConnect()
                return@setOnClickListener
            }

            if (viewModel.isScanning()) {
                viewModel.stopDeviceScan()
                return@setOnClickListener
            }

            getActivity()?.onBackPressed()
        }


        binding.display?.state = when {
            viewModel.isScanning() -> DeviceListDisplay.State.SCANNING
            viewModel.isConnecting() -> DeviceListDisplay.State.CONNECTING
            viewModel.isPairing() -> DeviceListDisplay.State.PAIRING
            else -> DeviceListDisplay.State.IDLE
        }

        binding.invalidateAll()

        viewModel.deviceListResult.observe(this, deviceListResultObserver)
        viewModel.deviceConnectedResult.observe(this, deviceConnectedResultObserver)
        viewModel.deviceScanDone.observe(this, deviceScanDoneObserver)


        viewModel.setupCallbacks()
        super.onViewCreated(view, savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DeviceListFragmentBinding.inflate(inflater, container, false)
        binding.display = DeviceListDisplay()
        binding.rvDevices.adapter = adapter
        binding.rvDevices.layoutManager = LinearLayoutManager(activity)
        return binding.root
    }

    fun onBackPressed(): Boolean {
        // TODO register this in activity

        if (viewModel.isConnecting()) {
            binding.display?.state = DeviceListDisplay.State.IDLE
            binding.invalidateAll()
            viewModel.cancelConnect()
            return true
        }

        if (viewModel.isScanning()) {
            viewModel.stopDeviceScan()
            return true
        }
        return true
    }


}