package io.surveystack.app.fragments.device

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.hardware.usb.UsbDevice
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import io.surveystack.app.R
import io.surveystack.app.app
import io.surveystack.app.arch.ConsumableObserver
import io.surveystack.app.databinding.DeviceSettingsFragmentBinding
import io.surveystack.app.device.bt.BtManager
import io.surveystack.app.device.usb.UManager
import org.json.JSONObject

class DeviceSettings : Fragment() {

    companion object {
        fun newInstance() = DeviceSettings()
    }

    private lateinit var viewModel: DeviceSettingsViewModel

    lateinit var binding: DeviceSettingsFragmentBinding

    private val connectedObserver = ConsumableObserver<Void> { _, consumed ->

        if (consumed) {
            return@ConsumableObserver
        }

        binding.display = DeviceInfoDisplay(
            "Device Information",
            text(),
            true,
            !viewModel.isConnected(),
            false
        )



        if (app().isOurSciDevice() && viewModel.isConnected()) {
            viewModel.fetchDeviceInfo()
        }

        binding.grpDeviceType.visibility = View.VISIBLE

        binding.invalidateAll()
    }

    private val jsonResultObserver =
        ConsumableObserver<JSONObject> { obj: JSONObject?, consumed: Boolean ->
            if (consumed || obj == null) {
                return@ConsumableObserver
            }


            binding.display = DeviceInfoDisplay(
                "Device Information",
                text() + "\n" + obj.keys().asSequence().toList()
                    .filter { obj.get(it) is String }
                    .joinToString("\n") {
                        when (obj.get(it)) {
                            is String -> "$it: ${obj.get(it)}"
                            else -> ""
                        }
                    },
                true,
                !viewModel.isConnected(),
                false
            )


        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.device_settings_fragment, container, false)
        return binding.root
    }

    fun text() = viewModel.currentDeviceInfo()?.let {
        when (it) {
            is BluetoothDevice -> {
                "status: ${if (viewModel.isConnected()) "connected" else "not connected"}\n" +
                        "device: ${it.name}\nMAC ${it.address}"
            }
            is UsbDevice -> {
                "status: ${if (viewModel.isConnected()) "connected" else "not connected"}\n" +
                        "USB vid/pid: %04X:%04X\n%s".format(
                            it.vendorId, it.productId, UManager.of(app()).info(
                                it.vendorId, it.productId
                            )
                        )
            }
            else -> "unknown device type"
        }

    } ?: if (viewModel.isConnected()) "connected, no information" else "not connected"


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        viewModel = ViewModelProvider(this).get(DeviceSettingsViewModel::class.java)
        viewModel.registerCallbacks()
        viewModel.jsonDataResult.observe(this, jsonResultObserver)
        viewModel.connectedResult.observe(this, connectedObserver)

        binding.btScan.setOnClickListener {
            findNavController().navigate(R.id.action_deviceSettings_to_navigation_device)
        }


        binding.btConnect.setOnClickListener {
            if (BtManager.of(app()).hasDevice()) {
                val adapter = BluetoothAdapter.getDefaultAdapter()
                if (adapter.state == BluetoothAdapter.STATE_OFF) {
                    val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    requireActivity().startActivityForResult(enableBtIntent, 0)
                    return@setOnClickListener
                }
            }

            binding.display = DeviceInfoDisplay(
                "Device Information", text(),
                true,
                !viewModel.isConnected(),
                true
            )

            binding.grpDeviceType.visibility = View.GONE
            binding.invalidateAll()


            viewModel.reconnect()
        }


        binding.btCancel.setOnClickListener {
            viewModel.disconnect()
        }


        binding.display = DeviceInfoDisplay(
            "Device Information",
            text(),
            true,
            !viewModel.isConnected(),
            viewModel.connecting()
        )

        binding.btDebug.setOnClickListener {
            findNavController().navigate(R.id.action_deviceSettings_to_deviceDebug)
        }

        binding.ivOursci.isChecked = app().isOurSciDevice()
        binding.ivGeneric.isChecked = !app().isOurSciDevice()

        //        binding.ivOursci.setOnCheckedChangeListener { buttonView, isChecked ->
        //            app().setOurSciDevice(isChecked)
        //            viewModel.fetchDeviceInfo()
        //
        //            binding.ivOursci.isChecked = app().isOurSciDevice()
        //            binding.ivGeneric.isChecked = !app().isOurSciDevice()
        //            binding.invalidateAll()
        //        }
        //

        binding.radioGroup.setOnCheckedChangeListener { group, checkedId ->
            app().setOurSciDevice(checkedId == R.id.iv_oursci)

            if (checkedId == R.id.iv_oursci) {
                viewModel.fetchDeviceInfo()
            }
        }

        binding.invalidateAll()

        if (app().isOurSciDevice() && viewModel.isConnected()) {
            viewModel.fetchDeviceInfo()
        }

        super.onViewCreated(view, savedInstanceState)
    }
}