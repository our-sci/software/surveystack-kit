package io.surveystack.app.fragments.settings

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import io.surveystack.app.R

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }
}