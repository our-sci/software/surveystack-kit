package io.surveystack.app.fragments.script

import android.app.Application
import android.text.Spanned
import android.webkit.JavascriptInterface
import io.surveystack.app.arch.ConsumableLiveData
import io.surveystack.app.base.AppViewModel
import io.surveystack.app.btlib.BluetoothService
import io.surveystack.app.btlib.BluetoothStatus
import io.surveystack.app.device.bt.BtManager
import io.surveystack.app.device.usb.UManager
import io.surveystack.app.html
import io.surveystack.app.webserver.WebServer
import io.surveystack.app.webserver.WebSocketServer
import org.json.JSONObject
import io.surveystack.app.measurement.MeasurementLoader
import org.oursci.android.common.measurement.MeasurementScript
import timber.log.Timber
import kotlin.concurrent.thread

class ScriptMainViewModel(application: Application) : AppViewModel(application) {


    val sensorResult = ConsumableLiveData<Void>()
    val sensorProgressResult = ConsumableLiveData<Float>()
    val sensorDataAvailable = ConsumableLiveData<Void>()
    val storeSuccessResult = ConsumableLiveData<Boolean>()
    val resultLoaded = ConsumableLiveData<Void>()
    val saveRequired = ConsumableLiveData<Void>()
    val deviceConnectedResult = ConsumableLiveData<String>()
    val cookieResult = ConsumableLiveData<Pair<String, String>>()
    val autoRunResult = ConsumableLiveData<String>()

    var currentMeasurementScript: MeasurementScript? = null

    val sensorInterface: SensorInterface = SensorInterface()
    val processorInterface: ProcessorInterface = ProcessorInterface()
    val applicationInterface: ApplicationInterface = ApplicationInterface()


    val consoleLog = StringBuffer(1000)


    var result: String? = null
    val resultLock = Object()
    var exportObject = JSONObject()
    var state: String? = null
    var autorun = true

    var dataBuffer = StringBuffer(1000)
    val dataLock = Object()

    val webInputData = StringBuffer(1000)


    inner class SensorInterface {
        fun getInputs(): String {
            val input = WebServer.of(app).mInput
            Timber.d("getting input $input")
            return input
        }

        @JavascriptInterface
        fun serialRead(): String {
            // Timber.d("serial read called of script")

            var toSubmit = ""
            synchronized(dataLock) {
                toSubmit = dataBuffer.toString()
                dataBuffer.setLength(0)
            }
            return toSubmit
        }

        @JavascriptInterface
        fun serialWrite(data: String) {
            Timber.d("writing data: %s", data)
            if (BtManager.of(app).connected.get()) {
                BtManager.of(app).write(data)
            } else if (UManager.of(app).connected.get()) {
                UManager.of(app).write(data)
            }
        }
    }

    inner class ProcessorInterface {
        @JavascriptInterface
        fun getResult(): String {
            Timber.d("getResult called")
            Timber.d("result is $result")
            synchronized(resultLock) {
                return result ?: ""
            }
        }
    }

    inner class ApplicationInterface {
        @JavascriptInterface
        fun getInputs(): String {
            return WebServer.of(app).mInput
        }


        @JavascriptInterface
        fun result(result: String) {
            Timber.d("result called")
            synchronized(resultLock) {
                this@ScriptMainViewModel.result = result
            }

            Timber.d("result is $result")

            sensorResult.postValue(null)
        }

        @JavascriptInterface
        fun progress(progress: Float) {
            Timber.d("progress %f", progress)
            sensorProgressResult.postValue(progress)
        }

        @JavascriptInterface
        fun measurementPath(): String {
            val dirPath = MeasurementLoader.of(app).measurementDir.absolutePath
            val path = currentMeasurementScript?.dir?.absolutePath?.replace(dirPath, "/files")
            return path ?: ""
        }

        @JavascriptInterface
        fun onSerialDataIntercepted(data: String) {

        }

        @JavascriptInterface
        fun csvExport(key: String, value: String) {
            Timber.d("csvExport %s => %s", key, value)
            exportObject.put(key, value)
        }

        /*
            Setting the state is optional.

            When a state is set, it will show up in the answer's meta object,
            and the measurement is considered "answered",
            if state is "success" (case insensitive).

            When no state is set, the meta object does not contain a state key,
            and is considered answered, when the answer is not blank. (legacy)

         */
        @JavascriptInterface
        fun setState(state: String?) {
            this@ScriptMainViewModel.state = state
        }


        @JavascriptInterface
        fun csvExport(value: String) {
            Timber.e("Dont use this anymore!")
            //exportBuffer.append("$value; ")
        }

        @JavascriptInterface
        fun save() {
            Timber.d("save called from script")
            saveRequired.postValue(null)
        }

        @JavascriptInterface
        fun getAnswer(dataName: String): String? {
            // TODO extract answer from submission
            return ""
        }

        @JavascriptInterface
        fun getCredentials(id: String): String {
            // TODO is this still necessary?
            return ""
        }

        // TODO maybe access to survey?
        @JavascriptInterface
        fun getMeta() = ""

        // TODO probably remove
        @JavascriptInterface
        fun getEnvVar(key: String?, default: String?): String {
            return ""
        }

        // TODO probably remove

        @JavascriptInterface
        fun setEnvVar(key: String?, value: String?, description: String?): Boolean {
            return true
        }

        @JavascriptInterface
        fun getWebInput(): String {
            val ret = webInputData.toString()
            webInputData.setLength(0)
            return ret
        }

        // TODO probably remove
        @JavascriptInterface
        fun email(to: String?, subject: String?, text: String?, cc: String?) {

        }

        // TODO probably remove
        @JavascriptInterface
        fun currentId(): String {
            return ""
        }
    }

    fun createMeasurementControl(id: String) {
        // TODO might need tweaking here
        // loadControlLiveData.postValue(null)
    }

    fun registerCallback() {
        BtManager.of(app).registerCallback({ _ ->

        }, { status ->
            when (status) {
                BluetoothStatus.CONNECTED -> {
                    Timber.d("status connected")
                    deviceConnectedResult.postValue("CONNECTED")
                }
                BluetoothStatus.CONNECTING -> {
                    Timber.d("status connecting")
                    deviceConnectedResult.postValue("CONNECTING")
                }
                BluetoothStatus.NONE -> {
                    Timber.d("status none")
                    deviceConnectedResult.postValue("NONE")
                }
            }
        }, { data ->
            synchronized(dataLock) {
                dataBuffer.append(data)
            }
            sensorDataAvailable.postValue(null)
        })

        UManager.of(app).registerCallback({ _ ->
        }, {}, { data ->
            synchronized(dataLock) {
                dataBuffer.append(data)

            }
            sensorDataAvailable.postValue(null)
        })
    }


    fun isConnected(): Boolean = if (UManager.of(app).hasDevice()) {
        UManager.of(app).connected.get()
    } else BluetoothService.getDefaultInstance().status == BluetoothStatus.CONNECTED


    fun connectToLastDevice(): Boolean {

        if (UManager.of(app).hasDevice()) {
            Timber.d("Trying to connect to USB device...")
            UManager.of(app).reconnect()
            return true
        } else if (BtManager.of(app).hasDevice()) {
            Timber.d("Trying to connect to Bluetooth device...")
            BtManager.of(app).reconnect()
            return true
        }

        return false
    }

    fun deviceName(): String? {
        return when {
            BtManager.of(app).hasDevice() -> BtManager.of(app).currentDevice?.name ?: "???"
            UManager.of(app).hasDevice() -> "USB device"
            else -> "???"
        }
    }

    fun cancelConnect() {
        UManager.of(app).clearDevice()
        BtManager.of(app).cancelConnect(null)
    }

    fun clearResult() {
        Timber.d("clear result called")
        return
    }

    fun clearBuffer() {
        dataBuffer.setLength(0)
    }

    fun saveResult(done: () -> Unit) {
        Timber.d("called saveResult...")

        val json = JSONObject()
        json.put("raw", JSONObject(result ?: ""))
        json.put("exported", exportObject)
        WebServer.of(app).publishResult(json.toString())
        WebSocketServer.of(app).done() {
            done()
        }
        // WebSocketServer.of(app).
    }

    fun saveResultSync(idx: Int): Boolean {
        var copyA = ""

        synchronized(resultLock) {
            copyA = result + ""
        }

        val copy = copyA
        result?.let {
            // TODO what to do with result? call back into web browser?
            return true
        } ?: return false
    }

    fun loadResultFromPath(filePath: String) {
        Timber.d("Load result from path");
        thread {
            val res = MeasurementLoader.of(app).loadMeasurementResult(filePath) ?: ""

            synchronized(resultLock) {
                result = res + ""
            }
            resultLoaded.postValue(null)
        }
    }

    fun appendLog(text: String) {
        consoleLog.append(text)
    }

    fun consoleLog(): Spanned {
        return html(consoleLog.toString())
    }

    fun requireDevice() = currentMeasurementScript?.requireDevice ?: true


    fun setupCallbacks() {
        registerCallback()
    }
}