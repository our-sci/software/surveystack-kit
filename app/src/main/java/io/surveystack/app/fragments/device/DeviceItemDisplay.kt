package io.surveystack.app.fragments.device

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.hardware.usb.UsbDevice
import android.view.View

data class DeviceItemDisplay(
    val device: Any,
    var title: String,
    val subTitle: String,
    val drawable: Drawable?,
    var state: State = State.UNPAIRED
) {
    enum class State {
        CONNECTED,
        CONNECTING,
        KNOWN,
        PAIRED,
        UNPAIRED,
        ACTIVE,
    }

    fun visibilityForgetButton() = when (state) {
        State.ACTIVE, State.KNOWN -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityPairButton() = when (state) {
        State.UNPAIRED -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityConnectButton() = when (state) {
        State.ACTIVE, State.KNOWN, State.PAIRED -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityDisconnectButton() = when (state) {
        State.CONNECTED -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityMoreButton() = when (state) {
        State.CONNECTED -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityIcon() = when (state) {
        State.ACTIVE, State.KNOWN, State.CONNECTED -> View.GONE
        else -> View.VISIBLE
    }

    fun cardBackgroundColor() = when (state) {
        State.CONNECTED -> Color.argb(100, 255, 255, 255)
        State.ACTIVE, State.KNOWN -> Color.argb(50, 255, 255, 255)
        else -> Color.argb(7, 255, 255, 255)

    }

    fun textHeader() = when (device) {
        is UsbDevice -> "USB Devices"
        else -> when (state) {
            State.KNOWN -> "Paired Devices"
            State.CONNECTED -> "My Devices"
            State.ACTIVE -> "Active Device"
            else -> "Other devices"
        }
    }

}



