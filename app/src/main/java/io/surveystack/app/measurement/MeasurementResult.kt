package org.oursci.android.common.measurement

/**
 * Created by Manuel Di Cerbo on 30.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
data class MeasurementResult(
        val measurement: MeasurementScript,
        val result: String)