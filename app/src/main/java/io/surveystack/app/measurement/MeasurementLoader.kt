package io.surveystack.app.measurement

import io.surveystack.app.base.App
import io.surveystack.app.utils.Compression
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.json.JSONObject
import org.oursci.android.common.measurement.MeasurementResultInfo
import org.oursci.android.common.measurement.MeasurementScript
import timber.log.Timber
import java.io.File
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Created by Manuel Di Cerbo on 30.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MeasurementLoader(val app: App) {

    var currentMeasurement: MeasurementScript? = null

    val measurementDir = File(app.appFilesDir, "measurement_scripts")


    companion object {
        private val lock = Object()


        private var sInstance: MeasurementLoader? = null

        fun of(app: App): MeasurementLoader {
            synchronized(lock) {
                if (sInstance == null) {
                    sInstance = MeasurementLoader(app)
                }

                return sInstance!!
            }
        }


        private fun isDirAndExists(dir: File) = dir.exists() && dir.isDirectory
        private fun isFileExistsAndReadable(file: File) =
            file.exists() && file.isFile && file.canRead()


        private fun manifest(dir: File) = File(dir, "manifest.json")
        private fun sensorScript(dir: File) = File(dir, "sensor.js")
        private fun processorScript(dir: File) = File(dir, "processor.js")


        private fun hasManifest(dir: File) = manifest(dir).let {
            isFileExistsAndReadable(it)
        }

        private fun hasSensorScript(dir: File) = sensorScript(dir).let {
            isFileExistsAndReadable(it)
        }

        private fun hasProcessorScript(dir: File) = processorScript(dir).let {
            isFileExistsAndReadable(it)
        }

    }


    val loading = AtomicBoolean(false)
    fun load(dir: File, onLoaded: (MeasurementScript) -> Unit, onError: (error: String) -> Unit) {

        when {
            !isDirAndExists(dir) -> {
                onError("script directory not found: ${dir.absolutePath}")
                return
            }

            !hasManifest(dir) -> {
                onError("manifest file not found: ${manifest(dir).absolutePath}")
                return
            }

            !hasSensorScript(dir) -> {
                onError("sensor script not found: ${sensorScript(dir).absolutePath}")
                return
            }
            !hasProcessorScript(dir) -> {
                onError("processor script not found: ${processorScript(dir).absolutePath}")
                return
            }
        }



        if (loading.getAndSet(true)) {
            return
        }

        try {
            val measurementScript = loadFromManifest(dir)
            copyToProperDir(dir, measurementScript)
            onLoaded(measurementScript)
        } catch (e: Exception) {
            Timber.e(e)
            onError("unable to load measurement: ${e.message}")
        }

        loading.set(false)
    }


    fun load(dir: File, date: Long?): MeasurementScript {
        when {
            !isDirAndExists(dir) -> {
                throw IllegalStateException("script directory not found: ${dir.absolutePath}")
            }

            !hasManifest(dir) -> {
                throw IllegalStateException("manifest file not found: ${manifest(dir).absolutePath}")
            }

            !hasSensorScript(dir) -> {
                throw IllegalStateException("sensor script not found: ${sensorScript(dir).absolutePath}")
            }
            !hasProcessorScript(dir) -> {
                throw IllegalStateException("processor script not found: ${processorScript(dir).absolutePath}")
            }
        }

        Timber.d("manifest modified")



        if (loading.getAndSet(true)) {
            throw IllegalStateException("already loading")
        }



        try {
            val measurementScript = loadFromManifest(dir, date)
            copyToProperDir(dir, measurementScript)
            loading.set(false)
            return measurementScript
        } catch (e: Exception) {
            Timber.e(e)
            loading.set(false)
            throw IllegalStateException("unable to load measurement: ${e.message}")
        }
    }


    private fun copyToProperDir(dir: File, measurementScript: MeasurementScript) {
        if (dir.absolutePath != measurementScript.dir.absolutePath) {
            measurementScript.dir.mkdirs()
            if (!dir.copyRecursively(measurementScript.dir, true)) {
                throw IllegalStateException(
                    "unable to copy scripts dir to proper locations" +
                            " ${dir.absolutePath}, ${measurementScript.dir.absolutePath}"
                )
            }

            val f = File(measurementScript.dir, "manifest.json")
            val o = File(dir, "manifest.json")
            if (f.exists() && o.exists()) {
                f.setLastModified(o.lastModified())
            }

        }
    }

    private fun createMeasurementScriptDir() {
        if (!measurementDir.exists()) {
            if (!measurementDir.mkdirs()) {
                throw IllegalStateException("unable to create measurement script dir: ${measurementDir.absolutePath}")
            }
        }
    }


    private fun loadFromManifest(dir: File, date: Long? = null): MeasurementScript {

        createMeasurementScriptDir()

        val json = JSONObject(manifest(dir).inputStream().bufferedReader().readText())


        return MeasurementScript(
            json.getString("id"),
            json.getString("name"),
            json.getString("description"),
            json.getString("version"),
            "",
            dirFromId(json.getString("id")),
            json.optString("action", null),
            json.optBoolean("requireDevice", true),
            date ?: manifest(dir).lastModified(),
            json
        )
    }


    private fun dirFromId(id: String) = File(measurementDir, "script_$id")
    fun fromId(id: String): MeasurementScript? {
        try {
            return loadFromManifest(dirFromId(id))
        } catch (e: Exception) {
            Timber.e(e)
        }
        return null
    }

    fun getAllMeasurementScripts(): Array<MeasurementScript> {
        Timber.d("measrurementDir ${measurementDir.absolutePath}")

        if (!measurementDir.exists()) {
            if (!measurementDir.mkdirs()) {
                Timber.e("unable to create measurement dir")
                return emptyArray()
            }
        }

        return measurementDir.list().mapNotNull {
            Timber.d("checking file $it")
            var f: File? = File(measurementDir, it)
            f?.isDirectory.let {
                if (it == false) {
                    f = null
                }
            }
            return@mapNotNull f
        }.mapNotNull {
            if (isDirAndExists(it) && hasManifest(it) && hasSensorScript(it)
                && hasProcessorScript(it)
            ) {
                try {
                    val measurementScript = loadFromManifest(it)
                    copyToProperDir(it, measurementScript)
                    return@mapNotNull measurementScript
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }

            return@mapNotNull null
        }.toTypedArray().reversedArray()
    }

    fun saveMeasurement(currentMeasurementScript: MeasurementScript, result: String): File? {
        try {
            createMeasurementScriptDir()
            val dir = dirFromId(currentMeasurementScript.id)
            val measurements = File(dir, "measurements")
            if (!measurements.exists()) {
                measurements.mkdirs()
            }
            val dateTime = ISODateTimeFormat.dateTime().print(DateTime())
            val fileName = "measurement_$dateTime.json"

            val out = File(measurements, fileName)
            out.outputStream().bufferedWriter().use {
                it.write(result)
            }

            Timber.d("success writing measurement file")
            return out
        } catch (e: Exception) {
            Timber.e(e)
        }

        return null


    }

    fun loadMeasurements(): Array<MeasurementResultInfo> {
        return getAllMeasurementScripts().flatMap { measurementScript ->
            val f = File(measurementScript.dir, "measurements")
            f.list()?.mapNotNull {
                File(f, it)
            }?.map {
                measurementScript to it
            } ?: emptyList()
        }.map { pair ->
            MeasurementResultInfo(pair.first, pair.second, pair.second.name)
        }.toTypedArray()
    }

    fun loadMeasurementResult(filePath: String): String? {
        val file = File(filePath)
        try {
            if (file.exists()) {
                file.inputStream().bufferedReader().use {
                    return it.readText()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

        return null
    }

    fun extract(f: File, date: Long? = null): MeasurementScript? {
        try {
            val tmpDir = File(app.cacheDir, "tmp-measurement-dir")
            tmpDir.deleteRecursively()
            tmpDir.mkdirs()

            Compression().unzip(f.inputStream(), tmpDir, date)

            return load(tmpDir, date)
        } catch (e: Exception) {
            Timber.e(e)
        }
        return null
    }


    fun resolveMeasurementFile(scriptId: String, fileName: String): File? {
        File(dirFromId(scriptId), "measurements").let {
            File(it, fileName).let {
                return if (it.exists()) it else null
            }
        }
    }


}