package io.surveystack.app

import android.annotation.SuppressLint
import android.app.Application
import android.os.Build
import android.preference.PreferenceManager
import android.text.Html
import android.text.Spanned
import androidx.fragment.app.Fragment
import androidx.lifecycle.AndroidViewModel
import io.surveystack.app.base.App
import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.PeriodFormatterBuilder
import timber.log.Timber
import java.io.File
import java.net.NetworkInterface

fun App.pref(key: Int): String? {
    return PreferenceManager.getDefaultSharedPreferences(this).getString(
        resources.getString(key), ""
    )
}

fun App.pref(key: Int, default: Int): String? {
    return PreferenceManager.getDefaultSharedPreferences(this).getString(
        resources.getString(key), resources.getString(default)
    )
}

fun App.storePref(key: Int, value: String) {
    PreferenceManager.getDefaultSharedPreferences(this).edit()
        .putString(resources.getString(key), value).apply()
}


fun App.booleanPref(key: Int, default: Boolean = false): Boolean {
    return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
        resources.getString(key), default
    )
}

fun App.ipAddresses(): Array<String> {
    try {
        return NetworkInterface.getNetworkInterfaces().toList().flatMap {
            it.inetAddresses.toList().filter { !it.isLoopbackAddress }.mapNotNull {
                if (it.address.size <= 4) {
                    it.hostAddress
                } else null
            }
        }.toTypedArray()
    } catch (ex: Throwable) {
        Timber.e(ex)
    }

    return arrayOf()
}

fun App.extractFile(assetName: String, dest: File) {
    dest.outputStream().use {
        val out = it
        assets.open(assetName).use {
            it.copyTo(out)
        }
    }
}

@SuppressLint("InlinedApi")
fun html(text: String): Spanned = when {
    Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> Html.fromHtml(
        text,
        Html.FROM_HTML_MODE_LEGACY
    )
    else -> Html.fromHtml(text)
}

fun Fragment.app(): App {
    return requireContext().applicationContext as App
}


private val formatter = PeriodFormatterBuilder()
    .appendYears().appendSuffix("#years ")
    .appendMonths().appendSuffix("#months ")
    .appendWeeks().appendSuffix("#weeks ")
    .appendDays().appendSuffix("#days ")
    .appendHours().appendSuffix("#h ")
    .appendMinutes().appendSuffix("#min ")
    .appendSeconds().appendSuffix("#s ")
    .printZeroNever()
    .toFormatter()


fun Long.timeAgo(): String {
    val period = Period(DateTime(this), DateTime())
    return formatter.print(period).split(" ").take(2).joinToString(" ") {
        it.split("#").joinToString(" ")
    }.let { "%s ago".format(it) }
}